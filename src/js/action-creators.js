export function addItem(name, value, purchaser) {
    var newItem = {name: name || '', value: value || '', purchaser: purchaser || ''};
    return {
        type: 'ADD_ITEM',
        value: newItem
    }
}

export function editItem(id, newItem) {
    return {
        type: 'EDIT_ITEM',
        id: id,
        item: newItem
    }
}

export function removeItem(id) {
    return {
        type: 'REMOVE_ITEM',
        id: id
    }
}

export function addPerson(name) {
    return {
        type: 'ADD_PERSON',
        name: name
    }
}