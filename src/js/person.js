
import _ from 'lodash';
import React from 'react';
import ReactIntl from 'react-intl';


var FormattedNumber = ReactIntl.FormattedNumber;
/*

                <div>
                    <span>subTotal: <FormattedNumber value={this.calculateSubTotal()} format="USD" />
                    </span>
                </div>
                <div> 
                    <span>total: <FormattedNumber value={this.calculateTotal(this.props.addedPercentage)} format="USD" />
                    </span>
                </div>
*/
var Person = React.createClass({
    calculateSubTotal: function() {
        return _.reduce(this.props.itemsBought, function(memo, item){ 
            return memo + item.value; 
        }, 0);
    },
    calculateTotal: function(percentageOnTopOfTotal) {
        return this.calculateSubTotal() * (1 + percentageOnTopOfTotal);
    },
    render: function() {
        var self = this;
        return (
            <div className="col-md-6" id="person">
                <span>{this.props.name}</span>
                <p>{this.calculateSubTotal()}</p>
                <p>{this.calculateTotal(this.props.addedPercentage)}</p>
            </div>
        );
    }
});

module.exports = Person;