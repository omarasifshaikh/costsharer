
import _ from 'lodash';
import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import Recipt from './recipt.js';

let nextItemId = 0;
let nextPersonId = 0;

var ming = { name: "Ming", id: ++nextPersonId};
var zeke = { name: "Zeke", id: ++nextPersonId};
var sean = { name: "Sean", id: ++nextPersonId};
var jackson = { name: "Jackson", id: ++nextPersonId};

var redWine = {name: "red wine", value: 7, purchaser: ming, id: ++nextItemId};
var burger = {name: "burger", value: 11.27, purchaser: ming, id: ++nextItemId};
var veggieBurger = {name: "veggie burer", value: 10.20, purchaser: zeke, id: ++nextItemId};
var harpoon = {name: "beer - harpoon", value: 6, purchaser: sean, id: ++nextItemId};
var cider = {name: "cider", value: 7.50, purchaser: sean, id: ++nextItemId};
var CSRA_shot = {name: "csra", value: 75.00, purchaser: {}, id: ++nextItemId};

var items = [redWine, burger, veggieBurger, harpoon, cider, CSRA_shot];
var people = [ming, zeke, sean, jackson];


var intlData = {
    "locales": "en-US",
    "formats": {
        "number": {
            "USD": {
                "style": "currency",
                "currency": "USD",
                "minimumFractionDigits": 2,
                "maximumFractionDigits": 2
            },
        }
    }
};

var initialState = {
    items: items,
    people: people
};
var itemReducer = function(state = initialState, action) {
    switch (action.type) {
        case 'ADD_ITEM':
            var nextItem = action.value;
            nextItem.id = ++nextItemId;
            return {
                ...state,
                items: state.items.concat(nextItem)
            };
        case 'EDIT_ITEM':
            var editIndex = _.findIndex(state.items, {id: action.id});
            var newItems = state.items.slice();
            if (editIndex != -1) {
                newItems[editIndex] = action.item;
            }
            return {
                ...state,
                items: newItems
            };
        case 'REMOVE_ITEM':
            var newItems = state.items.filter((item) => item.id != action.id);
            return {
                ...state,
                items: newItems
            };
        case 'ADD_PERSON':
            var nextPerson = {name: action.name, id: ++nextItemId};
            return {
                ...state,
                people: state.people.concat(nextPerson)
            };
        default:
            return state;
    }
};

var store = createStore(itemReducer);

ReactDOM.render(
    <Provider store={store}>
        <Recipt {...intlData} />
    </Provider>,
    document.getElementById('costSharer')
);
/*
var BetterListModel = function () {
var ming = new Person("ming");
ming.addItem("red wine", 7);
ming.addItem("burger", 11.27);

var zeke = new Person("zeke");
zeke.addItem("veggie burger", 10.20);

var sean = new Person("sean");
sean.addItem("beer - harpoon", 6);
sean.addItem("cider", 7.50);

this.recipt = new Recipt();
this.recipt.addPerson(sean);
this.recipt.addPerson(ming);
this.recipt.addPerson(zeke);

this.recipt.subTotal = ko.observable();//41.97
this.recipt.taxAmount = ko.observable();//1.67
this.recipt.tip = ko.observable();//9

this.subTotal = ko.observable(this.recipt.subTotal);
this.tax = ko.observable(this.recipt.taxAmount);
this.tip = ko.observable(this.recipt.tip);
this.allPeople = ko.observableArray(this.recipt.people);

console.log(this.recipt.calculateTaxPercent());
console.log(this.recipt.calculateTipPercent());
console.log(sean.calculateTotal(this.recipt.calculateTotalPercent()));
};

ko.applyBindings(new BetterListModel());
*/