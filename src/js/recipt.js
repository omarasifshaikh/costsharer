
import _ from 'lodash';
import React from 'react';
import ReactIntl from 'react-intl';
import { connect } from 'react-redux';

import * as actionCreators from './action-creators';
import Person from './person.js';
import Item from './item.js';


var IntlMixin = ReactIntl.IntlMixin;

var Recipt = React.createClass({
    getInitialState: function() {
        return {
            subTotal: 116.97,
            tax: 13,
            tip: 24
        };
    },
    calculateTaxPercent: function() {
        return (this.state.tax / this.state.subTotal);
    },
    calculateTipPercent: function() {
        // Assumes tip is off tax
        return (this.state.tip / (this.state.subTotal + this.state.tax));
    },
    calculateTotalPercent: function() {
        return this.calculateTaxPercent() + this.calculateTipPercent();
    },
    addItem: function(event) {
        event.preventDefault();
        this.props.addItem();
    },
    addPerson: function(event) {
        event.preventDefault();
        this.props.addPerson(event.target.elements.personName.value);
    },
    getItemsBoughtForPerson: function(person, items) {
        return _.filter(items, function(currentItem) {
            return currentItem.purchaser.id == person.id;
        });
    },
    handleSubTotalChange: function(event) {
        this.setState({subTotal: event.target.valueAsNumber});
    },
    handleTaxChange: function(event) {
        this.setState({tax: event.target.valueAsNumber});
    },
    handleTipChange: function(event) {
        this.setState({tip: event.target.valueAsNumber});
    },
    mixins: [IntlMixin],
    render: function() {
        var self = this;
        return ( 
            <div className="container-fluid">
                <div className="row panel panel-default" id="totals">
                    <p>subTotal <input type="number" value={this.state.subTotal} onChange={this.handleSubTotalChange} /></p>
                    <p>tax <input type="number" value={this.state.tax} onChange={this.handleTaxChange} /></p>
                    <p>tip <input type="number" value={this.state.tip} onChange={this.handleTipChange} /></p>
                </div>
                <div className="row panel panel-default" id="peopleList">
                    {this.props.people.map(function(person, i) {
                        return <Person name={person.name}
                                        itemsBought={self.getItemsBoughtForPerson(person, self.props.items)}
                                        addedPercentage={self.calculateTotalPercent()}
                                        id={person.id}
                                        key={person.id}
                                />;
                    })}
                    <form onSubmit={this.addPerson}>
                        <input id='personName' type='text' placeholder='name' />
                        <button>+ person</button>
                    </form>
                </div>
                <div className="row panel panel-default" id="itemsList">
                    {this.props.items.map(function(item, i) {
                        return <Item name={item.name} 
                                        value={item.value} 
                                        purchaserName={item.purchaser.name || ''}
                                        purchaserId={item.purchaser.id || -1}
                                        people={self.props.people}
                                        editItem={self.props.editItem}
                                        removeItem={self.props.removeItem}
                                        id={item.id}
                                        key={item.id}
                                />;
                    })}
                    <form onSubmit={this.addItem}>
                        <button>+ item</button>
                    </form>
                </div>
            </div>
        );
    }
});

export default connect(
    function(state) {
        return { 
            items: state.items,
            people: state.people
        };
    },
    function(dispatch) {
        return {
            addItem: (name, value, purchaser) => dispatch(actionCreators.addItem(name, value, purchaser)),
            editItem: (index, updatedItem) => dispatch(actionCreators.editItem(index, updatedItem)),
            removeItem: (index) => dispatch(actionCreators.removeItem(index)),

            addPerson: (name) => dispatch(actionCreators.addPerson(name))
        };
    }
)(Recipt);

module.exports = Recipt;