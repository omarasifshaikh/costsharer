
import React from 'react';


var Item = React.createClass({
    handleChangeName: function(event) {
        var item = {name: event.target.value, value: event.target.valueAsNumber, id: this.props.id, 
                    purchaser: {name: this.props.purchaserName, id: this.props.purchaserId} };
        this.props.editItem(this.props.id, item);
    },
    handleChangeValue: function(event) {
        var item = {name: this.props.name, value: event.target.valueAsNumber, id: this.props.id, 
                    purchaser: {name: this.props.purchaserName, id: this.props.purchaserId} };
        this.props.editItem(this.props.id, item);
    },
    handleChangePurchaser: function(event) {
        var selectedPersonOption = event.target.selectedOptions[0];
        var item = {name: this.props.name, value: this.props.value, id: this.props.id, 
                    purchaser: {name: selectedPersonOption.innerText, id: selectedPersonOption.value} };
        this.props.editItem(item.id, item);
    },
    removeItem: function(event) {
        event.preventDefault();
        this.props.removeItem(this.props.id);
    },
    render: function() {
        var self = this;
        return (
            <div className="col-lg-6 panel panel-default input-group" id="item">
                <input type="text" className="form-control input-sm" defaultValue={this.props.name} placeholder='item name' onChange={this.handleChangeName} />
                <span className="input-group-btn" style={{width:'0px'}}></span>
                <input type="number" className="form-control input-sm" defaultValue={this.props.value} placeholder='price' onChange={this.handleChangeValue} />
                <span className="input-group-btn" style={{width:'0px'}}></span>
                <select className='form-control' id='peopleMenu' defaultValue={self.props.purchaserId} onChange={self.handleChangePurchaser}>
                    {this.props.people.map(function(person, i) {
                        return (<option key={person.id} value={person.id}>
                                    {person.name}
                                </option>);
                    })}
                </select>
                <span className="input-group-btn" style={{width:'0px'}}></span>
                <form className='btn-group' onSubmit={this.removeItem}>
                    <button className='btn btn-custom-red'>-</button>
                </form>
            </div>
        );
    }
});

module.exports = Item;